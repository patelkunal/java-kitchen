package org.coderearth.thinkinginjava.concurrency;

class LiftOffv2 implements Runnable {

	protected int countDown = 5; // Default
	private static int taskCount = 0;
	private final int id = taskCount++;

	public LiftOffv2() {
	}

	public LiftOffv2(int countDown) {
		this.countDown = countDown;
	}

	public String status() {
		return "#" + id + "(" + countDown + ") ";
	}

	public void run() {
		System.out.println("\nStarting thread #(" + id + ")");
		while (countDown-- > 0) {
			System.out.print(status());
			Thread.yield();
		}
		System.out.println("\nFinished thread (" + id + ")");
	}
}
