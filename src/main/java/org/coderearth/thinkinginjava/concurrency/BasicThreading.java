package org.coderearth.thinkinginjava.concurrency;

public class BasicThreading {

	public static void main(String[] args) {
		System.out.println("Simple Launcher !!");
		LiftOff simpleLauncher = new LiftOff();
		simpleLauncher.run();

		System.out.println("\n");
		System.out.println("Simple Launcher using Thread constructor !!");

		Thread threadLauncher = new Thread(new LiftOff());
		threadLauncher.start();

		System.out.println("\n");
		System.out.println("Simple Launcher using multiple Thread constructor !!");

		for (int i = 0; i < 5; i++) {
			new Thread(new LiftOff()).start();
		}
	}

}