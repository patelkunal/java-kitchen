package org.coderearth.thinkinginjava.concurrency;

public class NotifyTryout {
	public static void main(String[] args) {
		Object obj = new Object();
		System.out.println("abc");
		// obj.notify();
		System.out.println("df");
		synchronized (obj) {
			System.out.println("Before Release");
			// obj = new Object();
			obj.notify();
			System.out.println("After Release");
		}
	}
}
