package org.coderearth.thinkinginjava.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorThreading {

	public static void main(String[] args) throws Exception {
		ExecutorService simpleCachedExecutorService = Executors.newCachedThreadPool();
		for (int i = 0; i < 5; i++) {
			simpleCachedExecutorService.execute(new LiftOffv2(3));
		}
		Thread.sleep(1000);
		simpleCachedExecutorService.shutdown();

		System.out.println("New fixed size thread pool ######################################################");
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(2);
		for (int i = 0; i < 5; i++) {
			fixedThreadPool.execute(new LiftOffv2(3));
		}
		fixedThreadPool.shutdown();
	}

}
