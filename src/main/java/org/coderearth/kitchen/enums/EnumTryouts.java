package org.coderearth.kitchen.enums;

import java.util.stream.Stream;

/**
 * Created by kunal_patel on 1/16/17.
 */
public class EnumTryouts {

    public static void main(String[] args) {
        System.out.println(Priority.valueOf("LOW"));
        System.out.println(Priority.forValue("high"));
        System.out.println(Priority.forValue("HIGH"));
        System.out.println(Priority.forValue("h"));
    }


    private enum Priority {
        LOW(1), MEDIUM(2), HIGH(3), CRITICAL(4);

        private int priority;

        Priority(final int i) {
            this.setPriority(i);
        }

        Priority() {
            this.setPriority(1);
        }

        public void setPriority(final int priority) {
            this.priority = priority;
        }

        public static Priority forValue(String value) {
            return Stream.of(Priority.values())
                    .filter(v -> value.equalsIgnoreCase(v.toString()))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Unknown priority type!!"));
        }
    }


}
