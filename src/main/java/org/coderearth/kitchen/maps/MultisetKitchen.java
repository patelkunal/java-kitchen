package org.coderearth.kitchen.maps;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.HashMultiset;

/**
 * Created by kunal_patel on 28/10/15.
 */
public class MultisetKitchen {

	class Threat {
		private String threatName;
		private String pageId;

		public Threat(final String threatName) {
			this(threatName, null);
		}

		public Threat(String threatName, String pageId) {
			this.threatName = threatName;
			this.pageId = pageId;
		}

		@Override
		public String toString() {
			if (this.pageId == null) {
				return MoreObjects.toStringHelper(this).add("threatName", this.threatName).toString();
			}
			return MoreObjects.toStringHelper(this).add("threatName", this.threatName).add("pageId", this.pageId).toString();
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(this.threatName);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			Threat other = (Threat) obj;
			return Objects.equal(this.threatName, other.threatName);
		}
	}

	public void recipe3() {
		HashMultiset<Threat> threats = HashMultiset.create();
		threats.add(new Threat("threat1"));
		threats.add(new Threat("threat2"), 10);
		threats.add(new Threat("threat3"), 20);
		System.out.println(threats);
		threats.add(new Threat("threat2"), 90);
		System.out.println(threats);
		System.out.println(threats.size());
	}


	public void recipe1() {
		HashMultiset<String> threatsMap = HashMultiset.create();
		threatsMap.add("threat1");
		threatsMap.add("threat2", 10);
		threatsMap.add("threat3", 20);
		System.out.println(threatsMap);
		System.out.println(threatsMap.size());
		System.out.println(Integer.MAX_VALUE);
	}

	public void recipe2() {
		HashMultiset<String> threatsMap = HashMultiset.create();


	}

	public static void main(String[] args) {
		MultisetKitchen multisetKitchen = new MultisetKitchen();
		multisetKitchen.recipe3();
	}

}
