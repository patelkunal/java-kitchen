package org.coderearth.kitchen.concurrency;

public class ExecutorThreadService implements Runnable {

	public void run() {
		System.out.println("thread: " + Thread.currentThread().getId() + ", starting !!");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("thread: " + toString() + ", finished !!");
	}

}
