package org.coderearth.kitchen.concurrency;

import org.coderearth.kitchen.concurrency.ExecutorThread;

public class MainInvoker {

	public static void main(String[] args) {

		System.out.println("Main thread: " + Thread.currentThread().getName()
				+ " started !!");
		ExecutorThread t1 = new ExecutorThread(100);
		ExecutorThread t2 = new ExecutorThread(200);

		t1.run();
		t2.run();

		new Thread() {
			public void run() {
				System.out.println("thread: " + getName() + " started !!");
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("thread: " + getName() + " finished !!");
			};
		}.start();

		System.out.println("Main thread: " + Thread.currentThread().getName()
				+ " finished !!");
	}
}
