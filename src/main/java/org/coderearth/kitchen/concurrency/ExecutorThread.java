package org.coderearth.kitchen.concurrency;

public class ExecutorThread extends Thread {

	private static int counter;

	public ExecutorThread(final int intialValue) {
		// this.counter = intialValue;
	}

	@Override
	public void run() {
		System.out.println("Count is = " + ++counter);
		System.out.println("thread: " + getName() + ", starting !!");
		try {
			sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("thread: " + getName() + ", finished !!");
	}
}
