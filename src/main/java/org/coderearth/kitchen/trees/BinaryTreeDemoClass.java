package org.coderearth.kitchen.trees;

public class BinaryTreeDemoClass {

	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree();
		tree.insert(5);
		tree.insert(15);
		tree.insert(2);
		tree.insert(10);
		tree.insert(8);
		tree.insert(9);
		System.out.println("In-order traversal");
		tree.inorder();
		System.out.println("\nLevel-order traversal");
		tree.levelOrder();
		System.out.println("\nReverse-Level-order traversal");
		tree.reverseLevelOrder();
		
		System.out.println("\nMax = " + tree.findMax());
		System.out.println("\nSize = " + tree.findTreeSize());
		System.out.println("\nHeight = " + tree.findTreeHeight());
	}

}
