package org.coderearth.kitchen.trees;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BinaryTree {

	private Node root = null;

	public BinaryTree() {
		root = null;
	}

	public void inorder() {
		inorder(root);
	}

	private void inorder(Node node) {
		if (node == null)
			return;
		inorder(node.getLeft());
		System.out.print(node + " ");
		inorder(node.getRight());
	}

	public boolean insertAt(Node parent, int nodeValue) {
		return this.insertAt(parent, new Node(nodeValue));
	}

	public boolean insertAt(Node parent, Node node) {
		/*
		 * cases
		 * parent is null - then it is empty tree so update root reference with new node
		 * 
		 * store temporary node to hold node-vale where it should be replaced. and 
		 */
		return false;
	}

	private Node insert(Node node, Node newNode) {
		if (node == null) {
			node = newNode;
		} else {
			if (newNode.getData() <= node.getData())
				node.setLeft(insert(node.getLeft(), newNode));
			else
				node.setRight(insert(node.getRight(), newNode));
		}
		return node;
	}

	/**
	 * 
	 * @param nodeValue
	 */
	public void insert(int nodeValue) {
		this.root = this.insert(this.root, new Node(nodeValue));
	}

	public void levelOrder() {
		Queue<Node> q = new LinkedList<Node>();
		q.add(root);

		while (!q.isEmpty()) {
			Node tmp = q.poll();

			// process current node
			System.out.print(tmp + " ");

			if (tmp.getLeft() != null) {
				q.add(tmp.getLeft());
			}
			if (tmp.getRight() != null) {
				q.add(tmp.getRight());
			}
		}
		q.clear();
		q = null;
	}

	/**
	 * Not working
	 */
	public void reverseLevelOrder() {
		Queue<Node> q = new LinkedList<Node>();
		Stack<Node> s = new Stack<Node>();
		q.add(root);

		while (!q.isEmpty()) {
			Node tmp = q.poll();

			// process current node
			// System.out.print(tmp + " ");

			if (tmp.getLeft() != null) {
				q.add(tmp.getLeft());
			}
			if (tmp.getRight() != null) {
				q.add(tmp.getRight());
			}
			s.push(tmp);
		}
		q.clear();
		q = null;
		while (!s.isEmpty())
			System.out.print(s.pop() + " ");
	}

	public int findMax() {
		return findMax(this.root);
	}

	private int findMax(Node root) {
		int max = root.getData();
		if (root.getLeft() != null)
			max = Math.max(max, findMax(root.getLeft()));
		if (root.getRight() != null)
			max = Math.max(max, findMax(root.getRight()));
		return max;
	}

	public int findTreeSize() {
		return findTreeSize(this.root);
	}

	private int findTreeSize(Node root) {
		if (root == null)
			return 0;
		else
			return 1 + findTreeSize(root.getLeft()) + findTreeSize(root.getRight());
	}

	public int findTreeHeight() {
		return findTreeHeight(this.root);
	}

	private int findTreeHeight(Node root) {
		if (root == null)
			return 0;
		else {
			int leftHeight = findTreeHeight(root.getLeft());
			int rightHeight = findTreeHeight(root.getRight());
			return 1 + Math.max(leftHeight, rightHeight);
		}
	}

	static class Node {
		int data;
		Node left;
		Node right;

		Node(int node) {
			this.data = node;
			left = null;
			right = null;
		}

		public int getData() {
			return data;
		}

		public void setData(int data) {
			this.data = data;
		}

		public Node getLeft() {
			return left;
		}

		public void setLeft(Node left) {
			this.left = left;
		}

		public Node getRight() {
			return right;
		}

		public void setRight(Node right) {
			this.right = right;
		}

		@Override
		public String toString() {
			return String.valueOf(this.getData());
		}
	}
}
