package org.coderearth.kitchen.java8;

import com.google.common.base.Objects;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by kunal_patel on 5/31/16.
 */
public class GroupingBy {

    public static void main(String[] args) {
        final Map<Integer, List<Foo>> monthlyStream = getFooStream().collect(Collectors.groupingBy(Foo::getMonthId));
        System.out.println(monthlyStream);
        assert monthlyStream.get(1).size() == 2;
        assert monthlyStream.get(2).size() == 3;
        assert monthlyStream.get(3).size() == 1;
        assert monthlyStream.get(4).size() == 1;
        System.out.println(monthlyStream.get(2).stream().mapToInt(Foo::getCount).sum());


        getFooStream()
                .filter(e -> e.getMonthId() == 2)
                .sorted(Comparator.comparingInt(Foo::getCount).reversed().thenComparing(Foo::getName))
                .forEach(System.out::println);

        System.out.println("##############################");

        getFooStream()
                .filter(e -> e.getMonthId() == 2)
                .sorted(Comparator.comparingInt(Foo::getCount).reversed().thenComparing((e1, e2) -> e1.getName().compareToIgnoreCase(e2.getName())))
                .forEach(System.out::println);


    }

    private static Stream<Foo> getFooStream() {
        return Stream.of(
                    new Foo(1, 10, "apple", "fruit"),
                    new Foo(1, 11, "mango", "fruit"),
                    new Foo(2, 90, "potato", "veg"),
                    new Foo(2, 11, "tomato", "veg"),
                    new Foo(2, 10, "apple", "fruit"),
                    new Foo(2, 10, "Apple", "fruit"),
                    new Foo(3, 10, "mango", "fruit"),
                    new Foo(4, 10, "mango", "fruit")
            );
    }

    private static final class Foo {
        private int monthId;
        private int count;
        private String name;
        private String type;

        public Foo(int monthId, int count, String name, String type) {
            this.monthId = monthId;
            this.count = count;
            this.name = name;
            this.type = type;
        }

        public int getMonthId() {
            return monthId;
        }

        public int getCount() {
            return count;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Foo{");
            sb.append("monthId=").append(monthId);
            sb.append(", count=").append(count);
            sb.append(", name='").append(name).append('\'');
            sb.append(", type='").append(type).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

}
