package org.coderearth.kitchen.java8;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

/**
 * Created by kunal_patel on 12/19/16.
 */
public class FiltersTryout {

    public static void main(String[] args) {
        List<Pair<Integer, String>> nameIdPairList = Lists.newArrayList();
        nameIdPairList.add(Pair.of(1, "foo"));
        nameIdPairList.add(Pair.of(2, "bar"));
        nameIdPairList.add(Pair.of(3, "foobar"));
        nameIdPairList.add(Pair.of(4, "foobar-foo"));

        nameIdPairList.stream()
                .filter(aPair -> aPair.getLeft() == 3)
                .forEach(System.out::println);

        nameIdPairList.stream()
                .filter(aPair -> !(aPair.getLeft() == 3))
                .forEach(System.out::println);
    }

}
