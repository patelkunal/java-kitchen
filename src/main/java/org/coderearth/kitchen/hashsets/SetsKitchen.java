package org.coderearth.kitchen.hashsets;


import com.google.common.collect.Sets;

import java.util.HashSet;

public class SetsKitchen {


    public static void main(String[] args) {
        System.out.println("Serials to be deleted !!");
        System.out.println(Sets.difference(Sets.newHashSet(), Sets.newHashSet("SRI0001", "SRI0002")));
        System.out.println(Sets.difference(Sets.newHashSet("SRI0001"), Sets.newHashSet("SRI0001", "SRI0002")));
        System.out.println(Sets.difference(Sets.newHashSet("SRI0007"), Sets.newHashSet("SRI0001", "SRI0002")));
        System.out.println(Sets.difference(Sets.newHashSet("SRI0001", "SRI0002"), Sets.newHashSet("SRI0001", "SRI0002")));

        System.out.println();
        System.out.println("Serials to be ignored !!");
        System.out.println(Sets.difference(Sets.newHashSet("SRI0001", "SRI0002"), Sets.newHashSet()));
        System.out.println(Sets.difference(Sets.newHashSet("SRI0001", "SRI0002"), Sets.newHashSet("SRI0001")));
        System.out.println(Sets.difference(Sets.newHashSet("SRI0001", "SRI0002"), Sets.newHashSet("SRI0007")));
        System.out.println(Sets.difference(Sets.newHashSet("SRI0001", "SRI0002"), Sets.newHashSet("SRI0001", "SRI0002")));


    }

}
