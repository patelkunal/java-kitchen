package org.coderearth.kitchen;

import java.util.LinkedHashMap;
import java.util.Map;

public class URLParamDecoder {

	public static Map<String, String> parse(String input) {
		input = input.indexOf('?') == -1 ? input : input.substring(input.indexOf('?') + 1);
		Map<String, String> params = new LinkedHashMap<String, String>();
		String[] pairs = input.split("&");

		for (String aPair : pairs) {
			int index = aPair.indexOf('=');
			if (index > 0) {
				params.put(aPair.substring(0, index), aPair.substring(index + 1));
			}
		}

		return params;
	}

	public static void main(String[] args) {
		Map<String, String> params = parse("http://www.cleartrip.com/signin/service?username=test&pwd=test&profile=developer&role=ELITE&key=manager");
		for (String k : params.keySet()) {
			System.out.println(k + ": " + params.get(k));
		}
		System.out.println(params);
	}

}