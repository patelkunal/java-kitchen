package org.coderearth.kitchen;

import java.util.HashMap;

public class MainClass {

	static class Key {
		int x;
		int y;

		public Key(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

		@Override
		public String toString() {
			return "{" + x + ", " + y + "}";
		}
	}

	public static void main(String[] args) {

		// Try mutable keys behavior in hashmap
		HashMap<Key, String> map = new HashMap<Key, String>();
		Key key = new Key(0, 0);
		System.out.println(key.hashCode());
		map.put(key, "00");
		System.out.println(map);
		key.setX(100);
		System.out.println(key.hashCode());
		System.out.println(map);
		// map.put(new Key(0, 1), "01");
		// map.put(new Key(0, 2), "02");
		// System.out.println(map);
	}

}
