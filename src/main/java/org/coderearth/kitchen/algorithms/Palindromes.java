package org.coderearth.kitchen.algorithms;


public class Palindromes {

	public boolean isPalindrome(char[] input, int start, int end) {
		for (int i = start, j = end; i < j; i++, j--) {
			if (input[i] == input[j])
				continue;
			else
				return false;
		}
		return true;
	}

	public String findLongestPalindromeImproved(char[] input) {
		int longestPalindrome = 0;
		int longestPalindromeStart = 0;
		int longestPalindromeEnd = 0;
		// for odd palindrome
		for (int mid = 0; mid < input.length; mid++) {
			for (int left = mid, right = mid; left >= 0 && right < input.length; left--, right++) {
				if (input[left] == input[right]) {
					int currPalindromeLen = right - left + 1;
					if (currPalindromeLen > longestPalindrome) {
						longestPalindrome = currPalindromeLen;
						longestPalindromeStart = left;
						longestPalindromeEnd = right;
					}
				} else
					// not sure if we need to break here if no matching characters
					break;
			}
		}

		// for even palindrome
		for (int mid = 0; mid < input.length; mid++) {
			for (int left = mid, right = mid + 1; left >= 0 && right < input.length; left--, right++) {
				if (input[left] == input[right]) {
					int currPalindromeLen = right - left + 1;
					if (currPalindromeLen > longestPalindrome) {
						longestPalindrome = currPalindromeLen;
						longestPalindromeStart = left;
						longestPalindromeEnd = right;
					}
				} else
					// not sure if we need to break here if no matching characters
					break;
			}
		}

		return String.valueOf(input).substring(longestPalindromeStart, longestPalindromeEnd + 1);
	}

	public String findLongestPalindromeDynamicProgramming(char[] input) {
		int longestPalindromeStart = 0;
		int longestPalindromeEnd = 0;

		int length = input.length;

		if (input.length <= 1)
			return String.valueOf(input);

		boolean[][] table = new boolean[length][length];
		// printTable(table);

		int longestPalindrome = 0;
		// mark true for all one lenght substrings
		longestPalindrome = 1;
		for (int i = 0; i < length; i++) {
			table[i][i] = true;
		}
		// printTable(table);

		// check for all two length substrings
		for (int i = 0; i < length - 1; i++) {
			if (input[i] == input[i + 1]) {
				longestPalindrome = 2;
				longestPalindromeStart = i;
				longestPalindromeEnd = i + 1;
				table[i][i + 1] = true;
			}
		}
		// printTable(table);

		// check for length more than 2
		for (int len = 3; len < length + 1; len++) {
			for (int i = 0; i < (length - len + 1); i++) {
				int j = i + len - 1;
				if (input[i] == input[j]) {
					table[i][j] = true;
					int currPalindrome = j - i + 1;
					if (table[i + 1][j - 1] && currPalindrome > longestPalindrome) {
						longestPalindrome = currPalindrome;
						longestPalindromeStart = i;
						longestPalindromeEnd = j;
					}
				} else {
					table[i][j] = false;
				}
			}
		}
		// printTable(table);

		return String.valueOf(input).substring(longestPalindromeStart, longestPalindromeEnd + 1);
	}

	public String findLongestPalindromeNaive(char[] input) {
		int longestPalindrome = 0;
		int longestPalindromeStart = 0;
		int longestPalindromeEnd = 0;
		for (int i = 0; i < input.length; i++) {
			for (int j = i + 1; j < input.length; j++) {
				if (isPalindrome(input, i, j)) {
					int currPalindromeLen = j - i + 1;
					if (currPalindromeLen > longestPalindrome) {
						longestPalindrome = currPalindromeLen;
						longestPalindromeStart = i;
						longestPalindromeEnd = j;
					}
				}
			}
		}
		return (String.valueOf(input).substring(longestPalindromeStart, longestPalindromeEnd + 1));
	}

	public void printTable(boolean[][] table) {
		for (boolean[] row : table) {
			for (boolean cell : row) {
				System.out.print((cell == true ? 1 : 0) + " ");
			}
			System.out.println();
		}
		System.out.println("=========");
	}

	public static void main(String[] args) {
		Palindromes p = new Palindromes();
		System.out.println(p.findLongestPalindromeNaive("bananas".toCharArray()));
		System.out.println(p.findLongestPalindromeNaive("abba".toCharArray()));

		System.out.println(p.findLongestPalindromeImproved("bananas".toCharArray()));
		System.out.println(p.findLongestPalindromeImproved("forgeeksskeegfor".toCharArray()));

		System.out.println(p.findLongestPalindromeDynamicProgramming("abba".toCharArray()));

		System.out.println(p.isPalindrome("foofoo".toCharArray(), 0, 5));
		System.out.println(p.isPalindrome("foofoo".toCharArray(), 1, 2));
	}

}
