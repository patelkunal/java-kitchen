package org.coderearth.kitchen.pairs;

import org.apache.commons.lang3.tuple.Pair;

import java.time.LocalDate;

/**
 * Created by kunal_patel on 11/24/16.
 */
public class CommonsPairTryout {

    public static void main(String[] args) {
        Pair<LocalDate, LocalDate> servicePeriod = Pair.of(LocalDate.parse("2016-10-01"), LocalDate.parse("2016-12-01"));
        System.out.println(servicePeriod);
        System.out.println(servicePeriod.getLeft());
        System.out.println(servicePeriod.getRight());
    }

}
