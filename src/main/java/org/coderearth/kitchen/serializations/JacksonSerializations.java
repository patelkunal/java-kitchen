package org.coderearth.kitchen.serializations;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by kunal_patel on 7/15/15.
 */
public class JacksonSerializations {

	private class Pojo {

		private String username;

		@JsonIgnore
		private String password;

		private String companyName;

		public Pojo(String username, String password, String companyName) {
			this.username = username;
			this.password = password;
			this.companyName = companyName;
		}

		public String getCompanyName() {
			return companyName;
		}

		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}
	}

	private Pojo p = null;

	public void createPojo() {
		p = new Pojo("kunal_patel", "password", "symantec");
	}

	public Pojo getPojo() {
		return this.p;
	}

	public void printPojo() {
		System.out.println(p);
	}

	public static void main(String[] args) throws JsonProcessingException {
		JacksonSerializations obj = new JacksonSerializations();
		obj.createPojo();
		obj.printPojo();

		obj.getPojo().setPassword("newpassword");

		System.out.println(new ObjectMapper().writeValueAsString(obj.getPojo()));

	}

}
