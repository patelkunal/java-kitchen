package org.coderearth.kitchen.puzzles;

import java.util.Arrays;

/**
 * http://www.geeksforgeeks.org/count-possible-paths-top-left-bottom-right-nxm-matrix/
 * 
 * Count number of possible paths in matrix while you can only go to right and down
 * 
 * @author kppatel
 *
 */
public class MatrixPathFinder {

	public void countPaths(int[][] matrix) {
		long startTime = System.currentTimeMillis();
		System.out.println(countPaths(matrix, 0, 0));
		System.out.println("Time taken by naive solution = " + (System.currentTimeMillis() - startTime));
		startTime = System.currentTimeMillis();
		System.out.println(countPathsUsingDP(matrix, 0, 0));
		System.out.println("Time taken by DP solution = " + (System.currentTimeMillis() - startTime));
	}

	private int countPathsUsingDP(int[][] matrix, int i, int j) {
		int len = matrix.length;
		if (i == len - 1 || j == len - 1) {
			matrix[i][j] = 1;
		}
		matrix[i + 1][j] = matrix[i + 1][j] == -1 ? countPaths(matrix, i + 1, j) : matrix[i + 1][j];
		matrix[i][j + 1] = matrix[i][j] == -1 ? countPaths(matrix, i, j + 1) : matrix[i][j + 1];
		matrix[i][j] = matrix[i + 1][j] + matrix[i][j + 1];
		return matrix[i][j];
	}

	private int countPaths(int[][] matrix, int i, int j) {
		int len = matrix.length;
		if (i == len - 1 || j == len - 1) {
			return 1;
		}
		return countPaths(matrix, i + 1, j) + countPaths(matrix, i, j + 1);
	}

	public static void main(String[] args) {
		int[][] matrix = new int[18][18];
		for (int[] row : matrix)
			Arrays.fill(row, -1);
		new MatrixPathFinder().countPaths(matrix);
	}
}
