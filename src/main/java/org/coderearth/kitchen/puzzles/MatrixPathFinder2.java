package org.coderearth.kitchen.puzzles;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MatrixPathFinder2 {

	static int[][] grid = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 }, { 16, 17, 18, 19, 20 }, { 21, 22, 23, 24, 25 } };
	static int size = 5;

	public static void main(String args[]) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		for (int i = 0; i < 5; i++) {
			String[] line = { "1", "22" };
			List<Integer> path = findPath(Integer.valueOf(line[0]), Integer.valueOf(line[1]));
			for (Integer p : path) {
				System.out.print(p + " ");
			}
			System.out.println();
			break;
		}
	}

	private static List<Integer> findPath(int source, int dest) {
		List<Integer> path = new ArrayList<Integer>();
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(source);
		path.add(source);
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();

		while (!q.isEmpty()) {
			int curr = q.poll();
			if (curr == dest) {
				int tmp = curr;
				while (true) {
					if (!map.containsKey(curr)) {
						return path;
					} else {
						tmp = map.get(tmp);
						path.add(tmp);
					}
				}
			}
			for (int i : findNeighbours(curr)) {
				q.add(i);
				map.put(i, curr);
			}
		}
		return new ArrayList<Integer>();
	}

	private static List<Integer> findNeighbours(int curr) {
		int i = 0;
		int j = 0;
		for (i = 0; i < 5; i++) {
			boolean flag = false;
			for (j = 0; j < 5; j++) {
				if (grid[i][j] == curr) {
					flag = true;
					break;
				}
			}
			if (flag)
				break;
		}
		List<Integer> path = new ArrayList<Integer>();
		if (j + 1 < size)
			path.add(grid[i][j + 1]);
		if (i + 1 < size)
			path.add(grid[i + 1][j]);
		if (j - 1 >= 0)
			path.add(grid[i][j - 1]);
		if (i - 1 >= 0)
			path.add(grid[i - 1][j]);
		return path;
	}

}