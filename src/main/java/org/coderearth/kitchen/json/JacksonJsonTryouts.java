package org.coderearth.kitchen.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.springframework.util.Assert;

import java.io.IOException;

/**
 * Created by kunal_patel on 8/19/16.
 */
public class JacksonJsonTryouts {

    public static void main(String[] args) throws IOException {
        ImmutableMap<String, String> map = ImmutableMap.of("name", "kunal", "role", "SE");
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(map);
        System.out.println(jsonString);

        JsonNode node = mapper.readTree(jsonString);
        System.out.println(node);
        System.out.println(node.get("name"));
        System.out.println(node.get("name1"));

        Assert.isTrue(map.containsKey("name"), "name doesnt exist !!");
        Assert.isTrue(node.get("name") != null, "name1 cannot be null !!");
    }

}
