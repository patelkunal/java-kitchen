package org.coderearth.kitchen.dates;

import java.sql.Date;
import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by kunal_patel on 9/8/16.
 */
public class DateTryouts {

    public static void main(String[] args) {
        final LocalDate endDate = Date.valueOf("2016-09-18").toLocalDate().atStartOfDay(ZoneId.of("UTC")).toLocalDate();
        LocalDate now = LocalDate.now(Clock.systemUTC());
        if (now.isAfter(endDate)) {
            System.out.println("Expired !!");
        } else {
            System.out.println(endDate);
        }

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(ZoneId.of("UTC")));
        System.out.println(cal);

    }

}
