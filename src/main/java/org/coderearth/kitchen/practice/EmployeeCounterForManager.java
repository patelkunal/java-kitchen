package org.coderearth.kitchen.practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class EmployeeCounterForManager {

	public HashMap<String, Integer> calculateDirectReportees(HashMap<String, String> dataset) {

		HashMap<String, ArrayList<String>> topDownHierarchyMap = createTopDownHierarchyMap(dataset);
		HashMap<String, Integer> result = new HashMap<String, Integer>();

		for (String employee : dataset.keySet()) {
			// System.out.println(result);
			this.countHelper(employee, topDownHierarchyMap, result);
			// System.out.println(result);
		}

		return result;
	}

	public HashMap<String, ArrayList<String>> createTopDownHierarchyMap(HashMap<String, String> dataset) {
		HashMap<String, ArrayList<String>> topDownHierarchyMap = new HashMap<String, ArrayList<String>>();
		for (Entry<String, String> entry : dataset.entrySet()) {
			ArrayList<String> empList = topDownHierarchyMap.get(entry.getValue());
			if (empList == null) {
				empList = new ArrayList<String>();
			}
			if (!entry.getKey().equals(entry.getValue()))
				empList.add(entry.getKey());
			topDownHierarchyMap.put(entry.getValue(), empList);
		}
		return topDownHierarchyMap;
	}

	public int countHelper(String emp, HashMap<String, ArrayList<String>> topDownHierarchyMap, HashMap<String, Integer> result) {
		ArrayList<String> employeeList = topDownHierarchyMap.get(emp);
		if (employeeList == null) {
			result.put(emp, 0);
			return 0;
		} else if (result.containsKey(emp)) {
			System.out.println("from cache => " + emp + ":" + result.get(emp));
			return result.get(emp);
		} else {
			// System.out.println(emp + " " + employeeList);
			int count = employeeList.size();
			for (String e : employeeList) {
				count += countHelper(e, topDownHierarchyMap, result);
			}
			result.put(emp, count);
			return count;
		}
	}
}
