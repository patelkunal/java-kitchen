package org.coderearth.kitchen.arrays;

import java.util.Arrays;

public class IntegerArrays {

	public static void main(String[] args) {
		int[][] matrix = new int[2][2];
		for (int[] row : matrix) {
			Arrays.fill(row, -1);
			System.out.println(Arrays.toString(row));
		}
	}
}
