package org.coderearth.kitchen.strings;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by kunal_patel on 8/17/16.
 */
public class GuavaStringsTests {

    @Test
    public void testCheckNullAndEmptyStrings() {
        assertTrue(StringUtils.isBlank(""));
        assertTrue(StringUtils.isBlank(" "));
        assertTrue(StringUtils.isBlank(null));
        assertFalse(StringUtils.isBlank("abc"));
        assertFalse(StringUtils.isBlank(" b"));
        assertFalse(StringUtils.isBlank("asdf asdf"));
    }

}
