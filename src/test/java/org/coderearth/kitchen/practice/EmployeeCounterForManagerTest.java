package org.coderearth.kitchen.practice;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class EmployeeCounterForManagerTest {

	@Test
	public void testCalculateDirectReportees() {
		HashMap<String, String> dataset = new HashMap<String, String>();
		dataset.put("A", "C");
		dataset.put("B", "C");
		dataset.put("C", "F");
		dataset.put("D", "E");
		dataset.put("E", "F");
		dataset.put("F", "F");

		HashMap<String, Integer> reportees = new EmployeeCounterForManager().calculateDirectReportees(dataset);
		System.out.println(reportees);
	}

	@Test
	public void testCreateTopDownHierarchyMap() {
		HashMap<String, String> dataset = new HashMap<String, String>();
		dataset.put("A", "C");
		dataset.put("B", "C");
		dataset.put("C", "F");
		dataset.put("D", "E");
		dataset.put("E", "F");
		dataset.put("F", "F");

		HashMap<String, ArrayList<String>> topDownHierarchyMap = new EmployeeCounterForManager().createTopDownHierarchyMap(dataset);
		System.out.println(topDownHierarchyMap);
	}

	@Test
	public void testCountHelper() {
		HashMap<String, ArrayList<String>> topDownHierarchyMap = Maps.newHashMap();
		topDownHierarchyMap.put("C", Lists.newArrayList("A", "B"));
		topDownHierarchyMap.put("E", Lists.newArrayList("D"));
		topDownHierarchyMap.put("F", Lists.newArrayList("C", "E", "X"));
		HashMap<String, Integer> result = new HashMap<String, Integer>();
		System.out.println(new EmployeeCounterForManager().countHelper("F", topDownHierarchyMap, result));
		System.out.println(new EmployeeCounterForManager().countHelper("C", topDownHierarchyMap, result));
		System.out.println(new EmployeeCounterForManager().countHelper("E", topDownHierarchyMap, result));
	}
}
